<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the LIVE MBO MODULE
 */

/**
 * Form builder
 * Implements system_settings_form().
 */

function mind_body_online_block_admin_settings(){
 	$form['mbo_creds'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mind Body Online Credentials'),
    '#description' => t('Please specify your Mind Body Online Credentials.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  	);
  
	$form['mbo_creds']['mind_body_online_block_sourcename'] = array(
	'#type' => 'textfield',
	'#title' => t('Sourcename'),
	'#description' => t('Enter the Mind Body Online Sourcename'),
	'#default_value' => variable_get('mind_body_online_block_sourcename', NULL),
	'#size' => 20,
	'#required' => TRUE,
	);
	
	$form['mbo_creds']['mind_body_online_block_password'] = array(
	'#type' => 'textfield',
	'#title' => t('Password'),
	'#description' => t('Enter the Mind Body Online Password'),
	'#default_value' => variable_get('mind_body_online_block_password', NULL),
	'#size' => 20,
	'#required' => TRUE,
	);

	$form['mbo_creds']['mind_body_online_block_siteid'] = array(
	'#type' => 'textfield',
	'#title' => t('Site ID'),
	'#description' => t('Enter the Mind Body Online Site ID'),
	'#default_value' => variable_get('mind_body_online_block_siteid', NULL),
	'#size' => 20,
	'#required' => TRUE,
	);
	
	$form['mbo_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mind Body Online Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  	);
	
	$form['mbo_settings']['mind_body_online_block_add_blocks'] = array(
	'#type' => 'textfield',
	'#title' => t('Add Blocks'),
	'#description' => t('Enter the number of blocks you would like to add'),
	'#default_value' => variable_get('mind_body_online_block_add_blocks', 1),
	'#size' => 4,
	'#required' => TRUE,
	);	
	
	return system_settings_form($form);
}

/**
 * Validate MBO fields submission.
 */

/**function mind_body_online_block_admin_settings_validate($form, $form_state){
	$mind_body_online_block_sourcename = $form_state['values']['mind_body_online_block_sourcename'];
	$mind_body_online_block_password = $form_state['values']['mind_body_online_block_password'];
	$mind_body_online_block_siteid = $form_state['values']['mind_body_online_block_siteid'];
	
	if ($mind_body_online_block_sourcename == NULL){
		form_set_error('mind_body_online_block_sourcename', t('Please enter a Sourcename'));
	}
	if ($mind_body_online_block_password == NULL){
		form_set_error('mind_body_online_block_password', t('Please enter a Password'));
	}
	if ($mind_body_online_block_siteid == NULL){
		form_set_error('mind_body_online_block_siteid', t('Please enter a SiteID'));
	}
}
 */
	$mbos = field_info_field('mind_body_online_block_sourcename');
	$mbop = field_info_field('mind_body_online_block_password');
	$mbosid = field_info_field('mind_body_online_block_siteid');
	
	
	
	